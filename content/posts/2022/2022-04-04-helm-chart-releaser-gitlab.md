---
title: "Helm Chart Releaser avec GitLab CI et Parent-Child Pipelines"
date: 2022-04-04T12:00:00Z
thumbnail: /2022/04/2022-04-04-gitlab.jpg
categories:
  - CI/CD
tags:
  - GitLab
  - Kubernetes
  - Bash
description: "Générez des charts Helm à l'aide de la CI/CD de GitLab et la fonctionnalité de pipelines parents/enfants"
toc: true
---

En tant qu'admin système, je gère quelques clusters Kubernetes, y déploie des applications, notamment avec Helm. J'utilise essentiellement GitLab comme plateforme de développement, avec la fonctionnalité d'Intégration et Déploiement Continue associée (GitLab CI).

## Introduction

Pour livrer quelques charts Helm, j'ai cherché quelque chose de similaire à ce que fait GitHub avec les actions et le [chart-releaser](https://github.com/helm/chart-releaser-action). J'ai trouvé quelques projets utilisant les GitLab Pages et [Chartmuseum](https://chartmuseum.com/), mais rien que ne me satisfasse.

J'ai alors décidé d'écrire mon propre GitLab CI helm-releaser, avec les spécifications suivantes :

- Le dépôt des charts Helm doit avoir la même structure que ceux que l'on trouve sur GitHub (comme ceux de Bitnami, Grafana, etc.)
- Le "releaser" de charts helm doit ne délivrer que les charts qui sont modifiés / mis à jour. La CI, en se déclenchant, ne doit pas tout régénérer.
- Les charts Helm doivent être mis à disposition sur [Harbor](https://goharbor.io/), dépôt d'images Docker et charts Helm, que j'utilise déjà.
- Pouvoir le déployer dans un environnement de staging Kubernetes.
- Optionnellement, pouvoir générer une release GitLab associée.

Ci-après quelques informations sur comment le chart releaser a été construit, avec les fonctionnalités de la CI GitLab et un peu de bash, comme d'habitude.

La fonctionnalité principale utilisée ici est de pouvoir générer des pipelines dynamiques, avec l'option "parent-child pipelines". Vous trouverez plus d'information ici : https://about.gitlab.com/blog/2020/04/24/parent-child-pipelines/

## Structure du dépôt GitLab

La structure du dépôt git ressemble basiquement à cela :

```sh
$ tree -aL 1
.
├── charts
├── gci-templates
├── .gitlab-ci.yml
└── scripts
```

1. Le dossier **charts** contient tous les charts.
2. Le dossier **gci-templates** contient les templates de jobs CI/CD.
3. Le dossier **scripts** contient les scripts permettant la génération automatique des jobs de CI.
4. Le fichier **.gitlab-ci.yml** classique pour utiliser la CI GitLab.

## Publication Helm avec la CI/CD

Publier un chart Helm est assez simple :

1. On valide le linting (visibilité / syntaxe du code) du chart.
2. On vérifie que l'installation peut passer avec un `--dry-run`.
3. On package le chart et on l'envoie sur le registre de charts Harbor (ou un  autre, au choix), avec un tag `dev` (voir la partie "Problèmes & Contournements" à ce propos).
4. On déploie une version correctement taggée sur Harbor (voir [semver](https://semver.org/)).
5. On crée une release GitLab.

Les pipelines de CI ne doivent se déclencher seulement si `charts/my-chart` est modifié. J'inclus donc un template pour éviter le copier/coller pour chaque chart que je dois générer.

*Note* : pour ce blog post, uniquement le lint & release sont affichées. Un pipeline plus complet est disponible à la fin de l'article.

Ce qui donne le code suivant dans le job de CI `.gitlab-ci.yml` :

```yaml
include: '/gci-templates/.gitlab-ci.yml'

stages:
  - test
  - install
  - release

'my-chart:lint':
  stage: test
  extends: .lint
  variables:
    HELM_NAME: "my-chart"
  rules:
    - if: '$CI_COMMIT_TAG =~ "/^$/"'
      changes:
        - charts/my-chart/**/*

'my-chart:release':
  stage: release
  extends: .release
  variables:
    HELM_NAME: "my-chart"
  rules:
    - if: '$CI_COMMIT_TAG =~ "/^$/"'
      changes:
        - charts/my-chart/Chart.yaml
```

Le template inclus ressemble à cela :

```yaml
---
image: dtzar/helm-kubectl:3.5.3 # last version using k8s 1.20

.lint:
  script:
    - helm lint .

.release:
  script:
    - apk add git
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm repo add --username=${DOCKER_REGISTRY_USER} --password=$(cat "$DOCKER_REGISTRY_PASSWORD") ${HELM_PROJECT} https://${DOCKER_REGISTRY}/chartrepo/${HELM_PROJECT}
    - helm package .
    - helm cm-push . ${HELM_PROJECT}
```

OK, maintenant que le job de CI est là, on peut publier le chart... Mais comme je n'ai pas envie de récrire les tâches de CI à chaque fois, on va utiliser les pipelines générés !

## Generated Pipelines

Les pipelines générés sont très simples : il s'agit d'une tâche de CI qui génère d'autres tâches. On fait cela en 2 étapes :

1. On génère un fichier yalm qui inclut toutes les étapes de la CI qu'on doit effectuer. Ce fichier yaml peut être créé via des scripts (ici bash)

```yaml
chart-generator:
  stage: generate
  image: alpine:3.15
  script:
   - ./scripts/generate-pipeline.sh > generated-pipeline.yml
  artifacts:
    expire_in: 1 hour
    paths:
      - generated-pipeline.yml
  rules:
    - if: '$CI_COMMIT_TAG =~ "/^$/"'
      changes:
      - charts/**/*
```

Le script `generate-pipeline.sh` contient le code simple précédemment vu pour générer un seul chart :

```bash
#!/usr/bin/env bash

cat <<EOF
include: '/gci-templates/.gitlab-ci.yml'
stages:
  - test
  - install
  - release
EOF

for f in $(find charts/* -maxdepth 0 -type d)
do
  CHART_VERSION=$(grep "^version:" ${f}/Chart.yaml | cut -d' ' -f2-)
  CHART_RELEASE="${f##*/}-${CHART_VERSION}"
  cat <<EOF
'${f##*/}:lint':
  stage: test
  extends: .lint
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_TAG =~ "/^$/"'
      changes:
        - ${f}/**/*
'${f##*/}:release':
  stage: release
  extends: .release
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_TAG =~ "/^$/"'
      changes:
        - ${f}/Chart.yaml
EOF

done
```

2. Appelons ce pipeline généré dans la prochaine étape de la CI, avec 2 mots-clés :

   - **trigger** : pour appeler un pipeline "enfant".
   - **include artefact** : pour appeler l'artefact précédemment généré.

```yaml
chart-jobs:
  stage: generate
  needs:
    - chart-generator
  trigger:
    include:
      - artifact: generated-pipeline.yml
        job: chart-generator
    strategy: depend
  rules:
    - if: '$CI_COMMIT_TAG =~ "/^$/"'
      changes:
      - charts/**/*
```

Avec toutes ces étapes, un pipeline enfant sera déclenché à chaque fois qu'un chart sera créé ou modifié, sans devoir recopier toutes les tâches de la CI.

## Magic

Maintenant que l'on a tout ce qu'il faut pour ne pas réécrire les pipelines à chaque fois, on peut pousser autant de charts que l'on veut, il suffit juste de l'inclure dans le dépôt, et la CI fera le reste !

![GitLab Child Pipelines](/2022/04/2022-04-04-gitlab-pipelines.png)

## Remarques et Ressources

On retrouvera le "Helm Chart Releaser" quasiment complet ici, il suffit "juste" de modifier quelques variables. Bien-sûr, à tester avant utilisation ;)

https://gitlab.com/rverchere/helm-chart-release-example

Voir aussi la section `release_tag` pour pousser les releases sur directement sur GitLab !


"Last but not the least", il ne reste plus qu'un peu de GitOps pour déployer en prod, mais cela est une autre histoire ;)

### Problèmes & Contournements

Avec Helm, il n'y a pas de notion de tag "latest" comme dans Docker. Pour contourner cela, on utilisera avec Harbor le tag `O.0.0-dev`. Ainsi, à chaque nouvelle release orienté `dev` dans Harbor, on utilisera ce tag.
