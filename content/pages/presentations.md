---
title: 'Présentations'
date: 2023-03-09T00:00:00+00:00
url: /presentations/
---

Ci-après la liste des mes présentations publiques.

## 2024

- ✨ Du GPU dans mes conteneurs ! @ DevFest Toulouse 2024 : [Video](https://youtu.be/kbEeE0qZqIk?si=QkcBysAJ9FNwKlrE) | [Slides](https://presentations.verchere.fr/GPU_Containers_Devfest_Toulouse_2024/)
- Développer en JAVA Cloud Native en 2024, Unpopular Opinion ? @ Volcamp 2024 : [Video](https://youtu.be/4T2Wb8hS1sE?si=o6X7cUjEfxqCxV6w) | [Slides](https://noti.st/rverchere/b9KhIG)
- 2,5 ans de migrations en environnement "Cloud Native" quand on n'est ni une Startup, ni dans le CAC40 @ Cloud Native Lorient 2024 : [Slides](https://presentations.verchere.fr/Migration_Cloud_ETI_Meetup_Lorient_2024/)
- Dimensionnez correctement vos déploiements Kubernetes @ Devoxx France 2024 : [Video](https://youtu.be/_NRbER3iSFo?si=3un7UYXUM9F8udSp) | [Slides](https://presentations.verchere.fr/Requests_Limits_Devoxxfr_2024/)
- Dimensionnez correctement vos déploiements Kubernetes @ Marsjug 2024 : [Slides](https://presentations.verchere.fr/Requests_Limits_Marsjug_2024/)
- 2 ans de migrations en environnement "Cloud Native" quand on n'est ni une Startup, ni dans le CAC40 @ Snowcamp : [Slides](https://presentations.verchere.fr/Migration_Cloud_ETI_Snowcamp_2024/)

## 2023

- Dimensionnez correctement vos déploiements Kubernetes @ Open Source Experience : [Slides](https://presentations.verchere.fr/Requests_Limits_OSXP_2023/)
- 2 ans de migrations en environnement "Cloud Native" quand on n'est ni une Startup, ni dans le CAC40 @ Volcamp : [Slides](https://presentations.verchere.fr/Migration_Cloud_ETI_Volcamp_2023/)
- Vos pipelines de CI/CD efficaces et sans prise de tête @ VMUG UserCon 2023 : [Slides](https://presentations.verchere.fr/VMUG_CICD_2023)
- Optimisez vos Requests et Limits dans Kubernetes @ Meetup 2023 : [Slides](https://docs.google.com/presentation/d/1kgkpDs12PrrB7jI_SXX6gAYk3UrzVR4N_gh7UO5mZGM/edit?usp=sharing)
- La CI/CD facile (sur GitLab) avec R2Devops et to-be-continuous @ Meetup 2023 : [Slides](https://presentations.verchere.fr/Meetup_CICD_2023/)
- Comment bien foirer sa certification CK{A,S} @ KCD France 2023 : [Video](https://youtu.be/NHHlKoEdT2M) | [Slides](https://docs.google.com/presentation/d/e/2PACX-1vT5_3kJv2f-mjuAsd3ErDVQ2F037bJZbi_tnbjL88V26j_sIPwTSfQcrg4jw-EC-sryJHA8oSQZ1TSn/pub?start=false&loop=false&delayms=3000)
- Table Ronde Communautaire - Comment faciliter les contributions open source et mieux les évangéliser @ KCD France 2023 : [Video](https://youtu.be/nZtoTYx1sPU)
- Velero - Sauvegardez et Restaurez (proprement) vos applications Kubernetes @ Meetup 2023 : [Slides](https://presentations.verchere.fr/Backup_Velero_Meetup_2023/)
- Velero - Sauvegardez et Restaurez (proprement) vos applications Kubernetes @ Very tech Trip 2023 : [Video](https://verytechtrip.ovhcloud.com/fr/media/oembed/ab12555e844c15e375b9db8d77e92c8e/) | [Slides](https://presentations.verchere.fr/Backup_Velero_VTT_2023/)

## 2022

- Velero - Sauvegardez et Restaurez (proprement) vos applications Kubernetes @ DevOps D-Day 2022 : [Video](https://youtu.be/uIKaiZQxqkI) | [Slides](https://presentations.verchere.fr/Backup_Velero_DevOps_Dday_2022/)
- Velero - Sauvegardez et Restaurez (proprement) vos applications Kubernetes @ Volcamp 2022 : [Video](https://www.youtube.com/watch?v=LlryJwgRXf4) | [Slides](https://presentations.verchere.fr/Backup_Velero_Volcamp_2022/)
- Comment bien foirer sa certification CK{A,S} @ Camping des Speakers 2022 : [Slides](https://presentations.verchere.fr/Comment_bien_foirer_sa_certification_Camping_Speakers_2022/)

## 2021

- Comment contribuer à l’Open Source (ou pas) sans être éditeur de solutions Open Source : [Video](https://youtu.be/tkN_1tHcNNQ) | [Slides](https://presentations.verchere.fr/Opensource_Experience_2021/)

## 2019

- Meetup Ansible : [Video](https://youtu.be/R-_Vf2thlJM?t=737)
