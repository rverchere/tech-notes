---
title: "Notes KubeCon & CloudNativeCon Europe 2024"
date: 2024-03-23T23:59:59Z
thumbnail: /2024/03/2024-03-24-kubecon.jpg
categories:
  - Conférence
tags:
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence KubeCon & CloudNativeCon Europe 2024.
toc: true
---
Chaque année, la CNCF organise un événement européen autour des technologies Kubernetes & Cloud Native. Pour 2024, cela se passe en France, à Paris, au Parc des Expositions Porte de Versailles.

C'est une occasion à ne pas manquer étant donné que ce sont des technos sur lesquelles je travaille au quotidien, et surtout le moment de rencontrer partenaires, collègues, amis & l'ensemble de la communauté tech FR.

Comme nombreux "frenchies", j'ai tenté ma chance en postulant au CFP, et comme la quasi-majorité, je n'y suis allé qu'en simple visiteur ;)

Voici ci-dessous mes notes, on parlera en vrac : des keynotes & conférences, de l'événement, des rencontres... Bonne lecture !

## KubeTrain & Avant-Goût

La KubeCon en elle-même dure 3 jours, mais d'autres conf sont organisées autour : les [Rejekts](https://cloud-native.rejekts.io/) le week-end précédent, les collocated-event la veille, une journée pour les enfants le samedi... Ne pouvant me permettre une absence trop longue, je n'ai assisté qu'aux 3 jours "classiques" (et c'est déjà énorme !).

Pour s'y rendre, j'ai profité de l'organisation [Kubetrain](https://kubetrain.io/), qui permet aux gens de venir en train. Un départ était prévu depuis Aix-Marseille, grâce à [Frédéric Léger](https://kubetrain.io/departures/aix-marseille/), merci à lui !

![Kubetrain](/2024/03/2024-03-24-kubetrain.jpg)

Arrivé dans en début de soirée, j'ai pu rejoindre l'équipe OVHCloud & Suse, et voir les quelques personnes sur place. L'occasion également un échange de bon procédé avec [Nuno](https://twitter.com/nunixtech) ;)

![Chocolate Corsair](/2024/03/2024-03-24-chocolats-nuno.jpg)

## La découverte, les keynotes, les tendances

Le jour J, on nous conseille d'arriver tôt pour ne pas perdre du temps au niveau des badges... 7h30 me voilà déjà en place, avec quelques collègues lève-tôt !

![Récupération des badges](/2024/03/2024-03-24-badges.jpg)

Je ne connais pas l'endroit, c'est simplement immense. On nous annonce plus de 12000 personnes, et pourtant ça passe "large". On retrouve au 1er étage 4 grandes salles, et de grandes tables pour se poser. Au 2nd se trouve l'espace sponsors / exposants, au 3ème la grande salle pour les keynotes & le reste des conférences. Et enfin, le "rooftop" où se retrouvaient régulièrement les frenchies pour debrief ;)

J'ai eu beaucoup de mal à me repérer dans ce vaste espace, surtout au niveau des exposants, ce qui m'a parfois joué des tours (quelques conférences loupées - mauvais endroit, mauvais timing).

### Les keynotes

Chaque jour, la journée débute par plusieurs keynotes. Des news de la CNCF, des talks sponsors, des sujets de la communauté.

#### [Mercredi](https://www.cncf.io/blog/2024/03/20/kubecon-cloudnativecon-europe-2024-day-two-how-cloud-native-is-powering-the-ai-movement-and-other-news/)

Le **mercredi**, c'est **AI** ! Beaucoup de "buzzword" autour de l'AI, GenAI, GPU, LLM & co, assez étrange pour un 1er contact avec la KubeCon. Pourtant, ce que l'on comprend, c'est que l'intelligence artificielle fait partie de notre quotidien, et que Kubernetes est aujourd'hui la plateforme de-facto pour permettre des usages à l'échelle.

On retrouvera plus tard pas mal de conférences sur le sujet, notamment sur comment exploiter au mieux les GPUs dans un environnement Cloud Natif. À suivre, car mon client actuel a justement des problèmes de gestion de partage de GPUs pour ses utilisateurs.

![Keynote](/2024/03/2024-03-24-keynote1.jpg)

#### [Jeudi](https://www.cncf.io/blog/2024/03/21/kubecon-cloudnativecon-europe-2024-day-three-the-power-of-sustainable-computing/)

Le **jeudi**, c'est **sustainability** (et **end-users**, mais ça rime moins bien) ! Faire de la tech c'est bien, mais la faire de manière écoresponsable c'est mieux !

Après quelques retours sur l'utilisation des produits CNCF par les utilisateurs (Backstage en pole position), échanges & témoignages pour faire de la techno plus "verte". Je retiens notamment :

- La montée en puissance de  WASM, pour avoir des workloads beaucoup plus légers.
- La consolidation des applications VM & Conteneurs avec des solutions comme KubeVirt.
- La meilleure utilisation des ressources avec tout ce qui tourne autour de l'autoscaling & un meilleur monitoring.

![Enduser Commits](/2024/03/2024-03-24-enduser.jpg)

#### [Vendredi](https://www.cncf.io/blog/2024/03/22/kubecon-cloudnativecon-europe-2024-day-four-how-cloud-native-is-powering-the-ai-movement-and-other-news/)

Le **vendredi**, c'est **community** ! Je n'ai malheureusement pas pu y assister, mais grâce aux canaux Slack, Telegram & co de la communauté FR j'ai pu suivre de loin quelques annonces :

- L'annonce du programme #Kubestronautes, qui récompensent les certifiés ayant l'ensemble des certifications CKx. Bravo à mon copain [David](https://twitter.com/DavAuff) qui en fait partie !
- Les [contributor card](https://contribcard.clotributor.dev/)
- L'intervention de [Solomon Hykes](https://twitter.com/solomonstre) avec une très belle référence au cinéma FR ;)

![Kubestronautes](/2024/03/2024-03-24-kubestronautes.jpg)


Côté tendances, on l'aura compris, c'est **AI** ! Viennent ensuite tout ce qui tourne autour du **Platform Engineering** & DevX : Kubernetes devient une commodité pour gérer les besoins métiers. **WASM** est à suivre de près, avec **eBPF** qui explose, ainsi que la partie **Service Mesh** côté réseau.

## La communauté, les partenaires & la tech FR

Pour reprendre les mots de la keynote de Solomon, *"la vie c'est d'abord des rencontres"*, la KubeCon c'est l'occasion de rencontrer les acteurs de la tech, qui plus est française, et faire le tour des stands pour ~~récupérer des goodies~~ découvrir les projets.

![ClashLoopBackoff](/2024/03/2024-03-24-clashloopbackoff.jpg)

Avec plus de 200 exposants, impossible d'échanger avec tout le monde, alors en vrac quelques rencontres :

- L'espace **projets CNCF**, où étaient représentés les principaux projets, avec leurs mainteneurs. Je n'ai pas passé assez de temps ici, je pense qu'une prochaine fois je privilégierai ce coin. J'ai juste eu l'occasion d'échanger avec les mainteneurs de KubeVirt.
- L'équipe de **Rancher by Suse**, où j'ai pu enfin *toucher* [Bertrand Thomas](https://twitter.com/devprofr), avec qui je partage pas mal d'idées, et qui est comme moi ambassadeur R2Devops. J'ai eu droit à une démo de leur solution edge, pluggée avec [akri](https://github.com/project-akri/akri) pour gérer des devices type imprimantes.
- L'équipe de **Docker**, où j'ai pu échanger avec [Guillaume Lours](https://twitter.com/glours), qui cherche des retours d'expérience sur la migration Docker Compose vers Kubernetes. Si vous êtes dans ce cas-là, n'hésitez pas à le contacter !
- L'équipe **Isovalent**, notamment [Raphaël Pinson](https://twitter.com/raphink), qui proposaient des labs sur l'ensemble des solutions autour de Cilium. Cilium & eBPF qui avaient beaucoup le vent en poupe lors de cette conférence
- L'équipe de **[Cast.ai](https://cast.ai/)**, avec [Ferréol Hoppenot](https://www.linkedin.com/in/ferreolhoppenot/), qui nous propose des outils permettant d'optimiser les coûts de vos infra. Un peu comme Karpenter, mais en mieux ;)
- Les équipes d'**Athéo** & **Cloudeo**, originaires de l'Est de la France, où [Nicolas Streng](https://www.linkedin.com/in/nicolas-streng/) & [Louis Gromb](https://www.linkedin.com/in/lgromb/) ont osé venir s'exposer, et ont tout simplement cartonné, bravo à eux !

J'ai également pris un peu de temps pour déambuler autour des stands et me laisser "approcher" par les éditeurs, en féliciter d'autres pour leurs produits. Me vient en tête [Amplification](https://amplication.com/), [Robusta](https://home.robusta.dev/), [Logz.io](https://logz.io/).

Niveau employeur, c'était aussi l'occasion de voir "en vrai" les collègues que je ne vois que par webcam !

L'occasion aussi d'échanger avec [Christophe](https://www.linkedin.com/in/christophesauthier/) qui gère maintenant toute la partie formation et certification au sein de la CNCF, et rencontrer l'équipe de son ancienne boite Objectif Libre.

Évidemment, j'ai discuté avec d'autres anciens collègues, et d'autres "copains & copines de conf" que je croise régulièrement sur les salons, ne m'en voulez pas si je ne vous cite pas ;)

Enfin, on ne peut pas parler de la communauté FR sans mentionner le **rooftop**, coin très apprécié des frenchies pour prendre l'air et se retrouver, avec une dédicace à la team [CuistOps](https://www.twitch.tv/cuistops) que l'on retrouve le lundi soir en live autour des sujets Cloud Native !

![Rooftop - Crédits Q. Joly](/2024/03/2024-03-24-rooftop.jpg)

## Détails des conférences

Pendant 4 jours, je n'ai pas fait *que* discuter, j'ai quand même pris le temps d'assister à quelques conférences.
Avec un programme très dense, difficile de faire des choix. Si au départ j'ai été un peu déçu de quelques sujets où je n'apprends rien de "nouveau", j'ai vite changé mon fusil d'épaule et suis allé voir des sujets que je connais moins, pour mieux élargir mes horizons.

Je ne vais pas ci-après faire le détail de chaque conf, vous aurez plus vite fait de voir les replays. Simplement quelques notes sur les sujets qui ont pu me marquer, avec une 🌟 pour mes sessions préférées.

- [Architecting Resilience: Lessons from Managing 7K+ Kubernetes Clusters at Scale](https://kccnceu2024.sched.com/event/1YeLc/architecting-resilience-lessons-from-managing-7k-kubernetes-clusters-at-scale-kwanghun-choi-gyutae-bae-kakao).
  Avoir 1 cluster par zone peut poser des problèmes de dispo lorsqu'on, surtout lorsque tout n'est pas répliqué à 100%.
  Passage à une architecture multizone, mais problème de latence réseau à prendre en considération : ETCD, passage à full eBPF, meilleure configuration des GLSB.

- [Bloomberg's Journey to a Multi-Cluster Workflow Orchestration Platform](https://kccnceu2024.sched.com/event/1YeLy/bloombergs-journey-to-a-multi-cluster-workflow-orchestration-platform-yao-lin-reinhard-tartler-bloomberg).
  Problèmes de synchro de workflows sur plusieurs clusters.
  Pour résoudre cela, utilisation de [kine](https://github.com/k3s-io/kine) avec BDD postgres H-A en backend.
  À noter que Kine a été présenté lors d'une autre session, à suivre !

- [Supercharging Argo CD’s Manifest Generation Capabilities](https://kccnceu2024.sched.com/event/1Yhfc/supercharging-argo-cds-manifest-generation-capabilities-alexander-matyushentsev-akuity-leonardo-luz-almeida-intuit).
  Vous voulez utiliser d'autres trucs que kustomize, helm avec ArgoCD ? Utilisez les ConfigManagementPluginV2 !

- [Chart Your Course Like a Champion](https://kccnceu2024.sched.com/event/1YhgI/chart-your-course-like-a-champion-andrew-block-karena-angell-red-hat-joe-julian-stealth-startup-scott-rigby-independent).
  Quelques astuces pour bien écrire les charts, mais **surtout** pour qu'ils soient bien maintenableS (ajout d'infos dans `Chart.yaml`).

- 🌟 [Intro + Deep Dive: Kubernetes SIG Scalability](https://kccnceu2024.sched.com/event/1Yhgs/intro-deep-dive-kubernetes-sig-scalability-wojciech-tyczynski-google-shyam-jeedigunta-amazon-web-services).
  Présentation du SIG Scalability, avec l'ensemble des outils (par exemple [ClusterLoader2](https://github.com/kubernetes/perf-tests/tree/master/clusterloader2)) qui permettent de valider les chiffres annoncés, du genre "k8s v1.29 permet de gérer X pods sur Y nodes.

![SIG Scalability](/2024/03/2024-03-24-sigscalability.jpg)

- [Leveling up Wasm Support in Kubernetes](https://kccnceu2024.sched.com/event/1YeP7/leveling-up-wasm-support-in-kubernetes-matt-butcher-fermyon).
  Présentation des produits Spin, et surtout SpinKube, qui permet de lancer des workloads WASM dans Kubernetes. Outil à suivre de près !

- [Confidential Containers for GPU Compute: Incorporating LLMs in a Lift-and-Shift Strategy for AI](https://kccnceu2024.sched.com/event/1YePm/confidential-containers-for-gpu-compute-incorporating-llms-in-a-lift-and-shift-strategy-for-ai-zvonko-kaiser-nvidia).
  Comment partager le GPU lorsqu'un utilise des Confidentials Conteneurs : gestion du GPU Passthrough & co. Pas simple !

![Confidential Containers](/2024/03/2024-03-24-confidentialcontainers.jpg)

- [Enhancing Reliability Through Multi-Cluster Deployment: Leveraging the Power of Karmada](https://kccnceu2024.sched.com/event/1YhiE/enhancing-reliability-through-multi-cluster-deployment-leveraging-the-power-of-karmada-hongcai-ren-huawei-xiao-zhang-daocloud-ying-zhang-zendesk). Présentation de l'outil [karmada](https://karmada.io/).


- 🌟 [CNI: Recap and Update](https://kccnceu2024.sched.com/event/1YhiK/cni-recap-and-update-casey-callendrello-isovalent-tomofumi-hayashi-red-hat).
  Aperçu de la gestion du projet CNI, niveau organisation. Comment juste ajouter un verbe dans la définition d'une CNI peut être long et impacter beaucoup d'autres outils.

![CNI](/2024/03/2024-03-24-cni.jpg)

- [Scaling Service Mesh: Self Service Beyond 300 Clusters](https://kccnceu2024.sched.com/event/1YeSt/scaling-service-mesh-self-service-beyond-300-clusters-sumit-mathur-sushanth-kamath-a-intuit)
  Présentation de l'outil  [naavik](https://github.com/intuit/naavik) pour gérer la synchro des Customs Resources Istio.

-  [Observable Feature Rollouts with OpenTelemetry and OpenFeature](https://kccnceu2024.sched.com/event/1YeSC/observable-feature-rollouts-with-opentelemetry-and-openfeature-daniel-dyla-michael-beemer-dynatrace). Feature Flag + Métriques : beau combo pour faire des rollbacks quand ça se passe mal.


J'aurais aimé assister à d'autres conférences comme celle sur les [Vertical Pod Autoscalers](https://kccnceu2024.sched.com/event/1YePO/to-infinity-and-beyond-seamless-autoscaling-with-in-place-resource-resize-for-kubernetes-pods-aya-ozawa-cloudnatix-inc-kohei-ota-apple), mais parfois les salles étaient pleines ("*Room is full !!*), parfois j'étais pris par le temps à discuter sur les stands ou ailleurs.

Sur l'ensemble des confs, ce que j'ai le plus apprécié au final, c'est le retour des mainteneurs et toute l'organisation qui va avec les projets Open Source de grande ampleur. Autant faire du deep-dive sur un produit peut le faire chez soi, en lisant des docs et suivant des tutos youtube, autant comprendre le fonctionnement des **SIG** & **TAG**, ça doit être vécu !

## Derniers mots

Cet article est assez long, j'espère que j'ai pu un peu retranscrire ces 3 jours de conférence à ma façon, et si vous arrivez à lire jusque-là je vous en félicite ;)

Ce fut une expérience très enrichissante, pas forcément sur la partie technique, mais beaucoup sur la partie organisation et communauté.

Le soir était propice aux échanges plus informels, avec beaucoup de discussions. Je retiens 2 remarques qui m'ont particulièrement touchées :

1) Mes blog posts & conférences sont appréciées, je vais donc essayer de sortir un peu plus d'articles, sans me mettre la pression non plus.

2) Certains aimeraient avoir la même motivation d'apprentissage de nouvelles technos quand ils auront mon âge. Je ne me pensais pas si vieux que ça, mais je le prends bien ;)

Un grand merci à l'ensemble des personnes que j'ai pu croiser, et qui ont fait de mes quelques jours à Paris un agréable moment !

Prochain événement pour moi [DevoxxFR](https://www.devoxx.fr/) 2024 !

![Stands vides](/2024/03/2024-03-24-fin.jpg)

## Ils en parlent aussi

Retrouvez deux autres articles de blog à ce sujet :

- [Zwindler](https://blog.zwindler.fr/2024/03/22/kubecon-eu-2024-vendredi/)
- [Cockpit.io](https://blog.cockpitio.com/events/kubecon-eu-paris-2024/)
- *Demandez à rajouter le vôtre !*