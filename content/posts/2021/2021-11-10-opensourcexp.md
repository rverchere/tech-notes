---
title: "Conférence Open Source Experience 2021"
date: 2021-11-10T23:59:59Z
thumbnail: /2021/11/2021-11-10-opensourcexp.png
categories:
  - Conférence
tags:
  - OSXP2021
---

A l'occasion de l'Open Source Experience 2021, j'ai eu la chance de pouvoir présenter un sujet non technique : *Comment contribuer à l’Open Source (ou pas) sans être éditeur de solutions Open Source*

Cet exercice de 20 minutes était ma première vraie conférence en tant que speaker, avec un accueil plutôt positif du public. J'espère que cela ne sera pas la dernière ;)

Je retiens 3 points sur cette expérience :

1. le sujet était assez simple à présenter, car je n'ai parlé que de mon expérience, sans aucune démo technique à faire
1. 20 minutes c'est vraiment court, il faut aller à l'essentiel. Pour de sujets plus techniques, je recommande au moins 40 minutes
1. La préparation d'un talk est longue, même si le sujet est maitrisé, et les feedbacks sont toujours bons à prendre pour s'améliorer

Vous trouverez le replay de la présentation [ici](https://youtu.be/tkN_1tHcNNQ). Malheureusement il n'a été enregistré qu'en audio, vous n'y verrez pas ma tête ;)

Le support de présentation est quand à lui disponible [ici](https://presentations.verchere.fr/Opensource_Experience_2021/)
