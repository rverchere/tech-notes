---
title: "Accéder à une ressource externe via un Ingress Controller et Certificat TLS valide"
date: 2023-10-16T00:00:00Z
thumbnail: /2023/10/2023-10-16-ingress-external-https.png
categories:
  - Réseau
tags:
  - Kubernetes
  - IT
description: Accéder à une ressource externe via un Ingress Controller et Certificat TLS valide
toc: true
---

Pour un besoin particulier, j'ai dû chercher une solution pour accéder à du contenu d'un bucket S3 public, depuis le même domaine qu'une application tournant sur un cluster Kubernetes. Les données devant être accessibles en HTTPS avec un certificat valide.

Le bucket S3 étant chez [OVHcloud](https://www.ovhcloud.com/fr/), il existe des solutions pour cela, notamment [celle-ci](https://help.ovhcloud.com/csm/fr-public-cloud-storage-s3-static-website-https?id=kb_article_view&sysparm_article=KB0058097). Cependant, j'utilise Let's Encrypt pour gérer mes certificats, j'ai déjà un load balancer et tout ce qu'il faut sur mes clusters kubernetes, il y a surement un moyen de faire autrement ?

Dans cet article, je vais alors tenter de récupérer un fichier `git-cheat-sheet-education.pdf`, hébergé sur un bucket S3 public, mais via une URL d'un cluster kubernetes, `https://demo-ingress-s3.opsrel.io`. Voici alors une solution proposée parmi d'autres, qui utilisera les concepts suivants :

1) Bucket S3 avec données accessibles publiquement
2) Ingress Controller (ici [ingress-nginx](https://kubernetes.github.io/ingress-nginx/)) sur un cluster Kubernetes
3) Let's Encrypt avec la génération de certificat TLS valide
4) Un nom de domaine ;)

Ci-après un schéma simple du cas d'usage :

![Requête HTTP via Ingress Controller et service ExternalName](/2023/10/2023-10-16-ingress-external-https-schema.png)

Je ne traiterai pas ici la création d'un bucket S3 ni la gestion des ACLs. Chez OVHcloud, vous trouverez des informations [ici](https://help.ovhcloud.com/csm/fr-public-cloud-storage-s3-bucket-acl?id=kb_article_view&sysparm_article=KB0057088).

Je ne traiterai pas non plus l'installation d'ingress-nginx et Let's Encrypt !

Mais du coup, de quoi vais-je traiter ? Eh bien simplement du lien ingress-nginx -> bucket S3 grâce aux ressources [External Names](https://kubernetes.io/docs/concepts/services-networking/service/#externalname) de Kubernetes !


## Vérification des pré-requis

Avant d'aller plus loin, vérifions que mon ingress controller et Let's Encrypt sont bien installés :

```
$ helm list -n ingress-nginx
NAME         	NAMESPACE    	REVISION	UPDATED                                 	STATUS  	CHART              	APP VERSION
ingress-nginx	ingress-nginx	2       	2023-10-16 21:58:04.555376832 +0200 CEST	deployed	ingress-nginx-4.8.2	1.9.3  

$ kubectl get svc ingress-nginx -n ingress-nginx
NAME                                 TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                      AGE
ingress-nginx-controller-admission   ClusterIP      10.43.34.243   <none>          443/TCP                      5m17s
ingress-nginx-controller             LoadBalancer   10.43.12.211   74.220.23.199   80:30198/TCP,443:31639/TCP   5m17s

$ helm list -n cert-manager 
NAME        	NAMESPACE   	REVISION	UPDATED                                 	STATUS  	CHART               	APP VERSION
cert-manager	cert-manager	1       	2023-10-16 22:02:52.440865101 +0200 CEST	deployed	cert-manager-v1.13.1	v1.13.1    

$ kubectl get deploy,pods -n cert-manager
NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cert-manager              1/1     1            1           79s
deployment.apps/cert-manager-cainjector   1/1     1            1           79s
deployment.apps/cert-manager-webhook      1/1     1            1           79s

NAME                                           READY   STATUS    RESTARTS   AGE
pod/cert-manager-55657857dd-fw8f2              1/1     Running   0          79s
pod/cert-manager-cainjector-7b5b5d4786-fnbcd   1/1     Running   0          79s
pod/cert-manager-webhook-55fb5c9c88-kdktw      1/1     Running   0          79s

$ kubectl get clusterissuer
NAME               READY   AGE
letsencrypt-prod   True    22s
```

OK, j'ai bien mon controller ingress-nginx et cert-manager qui tournent, avec une IP publique `74.220.23.199`.

Je vais utiliser l'entrée DNS `demo-ingress-s3.opsrel.io` qui va pointer vers cette IP. Pour le moment, la résolution fonctionne bien, mais le certificat par défaut du cluster est présenté :

```
$ curl https://demo-ingress-s3.opsrel.io -vkI
*   Trying 74.220.23.199:443...
* Connected to demo-ingress-s3.opsrel.io (74.220.23.199) port 443 (#0)
* [...]
* Server certificate:
*  subject: O=Acme Co; CN=Kubernetes Ingress Controller Fake Certificate
*  start date: Oct 16 19:58:20 2023 GMT
*  expire date: Oct 15 19:58:20 2024 GMT
*  issuer: O=Acme Co; CN=Kubernetes Ingress Controller Fake Certificate
*  SSL certificate verify result: self-signed certificate (18), continuing anyway.
* [...]
< 
* Connection #0 to host demo-ingress-s3.opsrel.io left intact
```

Le certificat est bien celui par défaut, `Kubernetes Ingress Controller Fake Certificate`, car aucune ressource n'est associée à ce nom de domaine. 

Maintenant, passons à l'étape suivante, créons la ressource de type **Ingress** + **ExternalName**.

## Ingress et External Name

On va maintenant configurer correctement ce qu'il faut pour récupérer le document [git-cheat-sheet-education.pdf](http://demo-ingress-s3.auth-2bf97a4bab8c4d16969513edf37fcc15.storage.gra.cloud.ovh.net/git-cheat-sheet-education.pdf), mais via une URL à moi, avec un bon certificat ;)

### Service ExternalName

Kubernetes permet d'exposer des services au sein du cluster, pour joindre des ressources. Les plus connus sont les services de type `ClusterIP` ou `LoadBalancer`. Dans mon cas, je vais utiliser un service un peu particulier, `ExternalName`, qui permet d'aller chercher une ressource à l'extérieur du namespace où il se trouve, que ce soit dans le cluster (sur un autre ns) ou carrément en dehors du cluster (mon cas d'usage).

Avec cet `ExternalName`, je pourrai donc depuis le cluster accéder à une URL externe via ce service.

Voici le manifest yaml `demo-ingress-s3-upstream-svc.yaml` à appliquer :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: demo-ingress-s3-upstream
spec:
  # FQDN de mon bucket S3
  externalName: demo-ingress-s3.auth-2bf97a4bab8c4d16969513edf37fcc15.storage.gra.cloud.ovh.net
  type: ExternalName
```

```
$ kubectl apply -f demo-ingress-s3-upstream-svc.yaml
service/demo-ingress-s3-upstream created

$ kubectl get svc demo-ingress-s3-upstream
NAME                       TYPE           CLUSTER-IP   EXTERNAL-IP                                                                       PORT(S)   AGE
demo-ingress-s3-upstream   ExternalName   <none>       demo-ingress-s3.auth-2bf97a4bab8c4d16969513edf37fcc15.storage.gra.cloud.ovh.net   <none>    4s
```

Si maintenant je fais un port-forward sur ce service, je devrais pouvoir récupérer mon fichier.

```
$ kubectl port-forward svc/demo-ingress-s3-upstream 80
error: cannot attach to *v1.Service: invalid service 'demo-ingress-s3-upstream': Service is defined without a selector
```

Mmmm, ça ne veut pas marcher... En effet, le service n'a pas de selector et ne sait pas vers quel port/pod rediriger, ça ne fonctionnera pas ! Qu'à cela ne tienne, on va tenter depuis un pod à l'intérieur du cluster :

```
$ kubectl run debug --image=nginx # on lance un conteneur nginx tout simple, pour avoir un shell ;)
$ kubectl exec -it debug -- /bin/bash
root@debug:/# curl http://demo-ingress-s3-upstream/git-cheat-sheet-education.pdf -I
HTTP/1.1 412 Precondition Failed
Content-Type: text/html; charset=UTF-8
Content-Length: 7
X-Trans-Id: tx7dc078f4c724468e89f50-00652ee47d
X-Openstack-Request-Id: tx7dc078f4c724468e89f50-00652ee47d
Date: Tue, 17 Oct 2023 19:46:05 GMT
X-IPLB-Request-ID: 4ADC14C0:8FA4_5762BBC9:0050_652EE47D_5A23D18:1D243
X-IPLB-Instance: 48126
```

Ça ne fonctionne toujours pas... C'est parce que le serveur en face s'attend à avoir une requête vers le vhost du bucket, et pas `demo-ingress-s3-upstream`. On retente en spécifiant le vhost :

```
root@debug:/# curl http://demo-ingress-s3-upstream/git-cheat-sheet-education.pdf  -H "Host: demo-ingress-s3.auth-2bf97a4bab8c4d16969513edf37fcc15.storage.gra.cloud.ovh.net" -I
HTTP/1.1 200 OK
Content-Type: application/pdf
Etag: 1fcf70393d2aed298ef2c521126e8cd3
Last-Modified: Tue, 17 Oct 2023 19:23:45 GMT
X-Timestamp: 1697570624.72872
Accept-Ranges: bytes
Content-Length: 100194
X-Trans-Id: tx74195141890c422cba15c-00652ee50b
X-Openstack-Request-Id: tx74195141890c422cba15c-00652ee50b
Date: Tue, 17 Oct 2023 19:48:27 GMT
X-IPLB-Request-ID: 4ADC14C0:7995_3626E64B:0050_652EE50B_4400B4E:71FE
X-IPLB-Instance: 33617
```

Ok ! On a bien un `200 OK`, essayant de télécharger mon PDF (`Content-Type: application/pdf`).

Par contre, si j'essaie de récupérer le document en HTTPS, j'ai l'erreur suivante, ce qui est normal car le certificat présenté ne matche pas avec l'URL demandée :

```
root@debug:/# curl https://demo-ingress-s3-upstream/git-cheat-sheet-education.pdf -I
curl: (60) SSL: no alternative certificate subject name matches target host name 'demo-ingress-s3-upstream'
```

### Ingress

Maintenant, reste à paramétrer l'ingress pour faire le lien depuis l'extérieur, vers ce service spécifique.

On crée une ressource de type ingress, `demo-ingress-s3-upstream-ingress.yaml`, avec les informations suivantes :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    # Ressource cert-manager pour demander des certificat Let's Encrypt, non traité ici
    cert-manager.io/cluster-issuer: letsencrypt-prod
    # On définit le vhost pour éviter le problème vu plus haut
    nginx.ingress.kubernetes.io/upstream-vhost: demo-ingress-s3.auth-2bf97a4bab8c4d16969513edf37fcc15.storage.gra.cloud.ovh.net
  name: demo-ingress-s3-upstream
spec:
  ingressClassName: nginx
  rules:
  - host: demo-ingress-s3.opsrel.io
    http:
      paths:
      - backend:
          service:
            # On pointe vers le service externalName définit plus haut
            name: demo-ingress-s3-upstream
            port:
              # Par défaut, le lien upstream se fait en HTTP, port 80
              number: 80
        path: /
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - demo-ingress-s3.opsrel.io
    # On stocke le certificat TLS dans le secret associé
    secretName: demo-ingress-s3-cert
```

On applique le manifest et on vérifie que tout est bon :

```
$ kubectl apply -f demo-ingress-s3-upstream-ingress.yaml
ingress.networking.k8s.io/demo-ingress-s3-upstream created

$ kubectl get ingress,certificate                                                                                                        
NAME                                                 CLASS   HOSTS                       ADDRESS         PORTS     AGE
ingress.networking.k8s.io/demo-ingress-s3-upstream   nginx   demo-ingress-s3.opsrel.io   74.220.23.199   80, 443   2m19s

NAME                                               READY   SECRET                 AGE
certificate.cert-manager.io/demo-ingress-s3-cert   True    demo-ingress-s3-cert   2m19s
```

Ok, reste plus qu'à tester :

```
$ curl https://demo-ingress-s3.opsrel.io/git-cheat-sheet-education.pdf -vI 
*   Trying 74.220.23.199:443...
* Connected to demo-ingress-s3.opsrel.io (74.220.23.199) port 443 (#0)
* [...]
* Server certificate:
*  subject: CN=demo-ingress-s3.opsrel.io
*  start date: Oct 17 18:53:20 2023 GMT
*  expire date: Jan 15 18:53:19 2024 GMT
*  subjectAltName: host "demo-ingress-s3.opsrel.io" matched cert's "demo-ingress-s3.opsrel.io"
*  issuer: C=US; O=Let's Encrypt; CN=R3
*  SSL certificate verify ok.
* [...]
< HTTP/2 200
HTTP/2 200
< date: Tue, 17 Oct 2023 19:59:38 GMT
date: Tue, 17 Oct 2023 19:59:38 GM
< content-type: application/pdf
content-type: application/pdf
< content-length: 100194
content-length: 100194
[...]
```

Cool ! Le document peut être récupéré, en HTTPS, depuis un domaine que je gère depuis mon cluster !

Vous aurez remarqué que la requête entre mon poste et l'ingress est en HTTPS, mais depuis mon cluster vers le bucket S3 on reste en HTTP (`port.number: 80`). Pour faire propre, on met à jour la ressource ingress pour gérer le backend AUSSI en HTTPS :

```yaml
piVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/backend-protocol: HTTPS
[...]
spec:
  rules:
  - host: demo-ingress-s3.opsrel.io
    http:
      paths:
      - backend:
          service:
            name: demo-ingress-s3-upstream
            port:
              number: 443
[...]
```

Et voilà, Avec 2 ressources dans Kubernetes, vous pouvez maintenant récupérer des documents depuis un bucket S3 (ou tout autre document externe) avec vos propres URLs et certificats TLS.

## Notes

### Références

Lors de la rédaction de l'article, des confrères de Zenika ont sorti un article sur des possibilités de configuration de l'ingress-controller nginx. N'hésitez pas à aller le lire, c'est [ici](https://dev.to/zenika/kubernetes-nginx-ingress-controller-10-complementary-configurations-for-web-applications-ken).


### CIVO

En tant que "developer advocate" [Civo](https://www.civo.com/), je les remercie de me permettre d'utiliser leurs produits, notamment leur offre de service de cluster k8s managé, utilisée pour cet article.

![Logo CIVO](/2023/10/2023-10-16-civo-logo.png)
