---
title: "Notes Very Tech Trip 2023"
date: 2023-02-02T23:59:59Z
thumbnail: /2023/02/2023-02-02-vtt.png
categories:
  - Conférence
tags:
  - VTT
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence Very Tech Trip 2023.
toc: true
---

## Very Tech Trip

Organisée par OVHcloud, cette conférence réunit l'ensemble des acteurs de la tech FR, avec comme sujets tout ce qui touche de prêt ou de loin les solutions que proposent le cloud provider : Cloud Native, Infrastructure, Code, IA, Electronique, bref des sujets techs pour les techs !

Cette conférence se veut orientée tech & communautaire, avec des retours d'expérience plutôt que des speechs partenaires, contrairement à l'OVH Summit.

Very Tech Trip, c'est donc une journée de rex tech à la Cité des Sciences et de l'Industrie à Paris, par les techs, pour les techs, le tout dans une ambiance décontractée et la bonne humeur !

Toutes les vidéos sont disponibles ici : https://verytechtrip.ovhcloud.com/fr/sessions/

## La conférence, ambiance, environnement, etc.

Comme dit plus haut, la conférence c'est un jour de rex sur Paris, dans un bel endroit, et c'était la première édition !

Beaucoup de sessions étaient prévues, 12 thèmes, 10 salles (pour des confs, labs, lightning talks), ça fait beaucoup.

Arrivé dès l'ouverture, une marée de t-shirt bleus m'attendaient, OVHcloud était là en force ! J'ai pu alors profiter du calme avant les sessions pour échanger avec les responsables produits Managed Kubernetes, Developer Experience, les DevRels que je commencent à croiser souvent. Etant client OVHcloud, c'était l'occasion pour discuter IRL, remonter les problématiques "terrain", et en savoir plus sur leur roadmap. Toute l'équipe est très demandeuse de retour d'XP, surtout sur des sujets relativements nouveaux.

![Team DevRels](/2023/02/2023-02-02-vtt-devrels.jpg)

J'ai eu aussi la "faiblesse" de répondre affirmativement quand la veille, lors de la soirée des speakers, on m'a proposé de devenir "sponsor communautaire" et répondre à une interview, j'y reviendrai plus tard.

Ambiance détendue donc, et c'était aussi l'occasion pour moi de rencontrer les personnes que je suis sur les réseaux sociaux, et tout particulièrement la team [DevObs](https://devobs.p7t.tech/), qui aime les débats animés, mais qui n'est pas si obtue en vrai, avec une vraie expertise sur les sujets techs ;)

Place maintenant au sessions auxquelles j'ai pu assister, qui ne sont pas si nombreuses, pour diverses raisons : j'ai été speaker, interviewé, ayant passé pas mal de temps à discuter avec les teams OVHcloud et les autres speakers et techs. Le temps passe vite quand on est en bonne compagnie !

## Keynote d'ouverture

Organisateur de la conférence et principal sponsor oblige, Octave Klaba et les divers responsables des branches produits OVHcloud nous présentent chacun leur tour les différentes nouveautés et annoncent en fonction des divers segments.

Ca parle de Public Cloud, Databases, Serveurs dédiés, etc...

Je retiens parmi les annonces 2 choses qui m'intéressent:
- l'IAM qui va être bientôt dispo
- L'offre Managed Kubernetes Service Multi Cloud

Maintenant les annonces faites, on attend patiemment les offres GA ;)

![IAM Beta](/2023/02/2023-02-02-vtt-iam.jpg)

### Commentaires

> Au delà des annonces qui sont toujours bien à prendre, désolé la team OVHcloud mais si sur le font les informations étaient pertinentes, sur la forme je note des "axes d'amélioration". Les responsables produits avaient les mêmes mimiques, en surjouant, je pense qu'il faudrait vous rapprocher de vos DevRels pour un coaching speaker tech plus efficace ;)

## Velero : Sauvegardez et Restaurez (proprement) vos applications kubernetes

De suite après la keynote, j'ai eu le privilège de présenter un REX sur Velero.

La salle était remplie, 100 personnes à m'écouter religieusement comment tenter de bien restaurer des données sur un cluster kubernetes !

Après quelques problèmes techniques niveau HDMI, ça a bien enchainé, et les retours en fin de session est toujours intéressante. Peu de questions mais des partages d'expérience pour compléter mes propos, c'est cool !

C'était la 2ème fois que je donnais cette conf, plus à l'aise sur le sujet, donc de mon côté RAS, ça a bien déroulé !

## Entrevue

A peine fini ma conf, j'enchaine sur un entretien mené par William Dubreuil "Communities Experience Manager" chez OVHcloud, qui m'avait proposé la veille de répondre à quelques questions en live.

L'interview assez courte, les sujets tournaient autour de l'événement, OVHcloud et la tech en général. Je ne sais pas encore si les vidéos seront dispo, mais l'exercice était toujours bon à prendre !

On notera une volonté d'OVHcloud d'être plus proche de la communauté tech, on attend de voir ce que cela va donner.

## Et si, vous aussi, vous construisiez des robots ?

Stéphanie Moallic, front end dev sur les solutions de téléphonie d'OVHcloud, nous présente un truc qui n'a strictement rien à voir avec son job : les robots !

Elle nous partage sa passion, avec tous les différents types de robots qu'elle aime programmer & animer, son parcours robotique l'utilisation qu'elle en a avec les enfants.

Avec quelques démos live à la clé, elle nous présente ses petites bêtes, de la plus basique à la plus complexe, sans accrocs !

![Robots](/2023/02/2023-02-02-vtt-robots.jpg)

### Commentaires

> Avec "un peu de stress", on sentait quand même la passion qu'a Stéphanie avec ces petits jouets, et le plaisir qu'elle a à transmettre aux plus jeunes. Bravo!

> Et même si cela n'est pas mon sujet de prédilection, je me devais d'assister à sa conf pour la supporter (j'ai rencontré Stéphanie au Camping des Speakers, on se refait pas !)

## C'est quoi un Bastion et pourquoi vous devriez en utiliser un ?

Stéphane Lesimple d'OVHcloud nous présente la solution de bastion "The Bastion" made in OVHcloud, produit maison Open Source pour accéder à l'ensemble des serveurs que les admins gèrent.

Après avoir expliqué les différentes méthodes pour se connecter à des serveurs, notamment les problèmes que peuvent engendrer des connexions directes, Stéphane nous explique ce qu'est un bastion, à quoi cela sert, les problèmes qu'il résoud et comment justement a été pensé l'outil d'OVHcloud.

Avec une gestion "de masse" de serveurs, "The Bastion" répond bien au besoin, avec une expérience sysadmin (à défaut d'expérience développeur) la plus efficace possible, pour que l'utilisation de l'outil soit le plus transparent possible, avec une gestion de droits efficaces.

![Robots](/2023/02/2023-02-02-vtt-bastion.jpg)


### Commentaires

> L'utilisation d'un Bastion est quasi obligatoire quand on doit administrer des machines et gérer/tracer/auditer les accès.. Est-ce que l'outil rempli ce besoin ? Oui. 

> De mon côté, utilisant beaucoup Kubernetes ces temps-ci, j'aurai bien aimé savoir si le coup du HTTPS proxy fonctionne bien pour ce cas d'usage.

## Keynote de fin

Déjà fini les confs (et oui, le temps passe trop vite quand on est bien entouré), on termine par un échange de questions / réponses par l'équipe dirigeante d'OVHcloud.

Les questions ont été posées au fil de la journée par les participants, et l'équipe de modération en avait sélectionnée quelques-unes.

Parmi les sujets, un m'a particulièrement intéressé, à savoir la participation d'OVHcloud à l'écosystème Open Source : Octave a répondu qu'ils avaient une forte volonté d'y participer, mais que ce n'était pas si simple que cela. Mettre juste du code sur Github ne suffit pas, il faut bien packager les projets, avoir un vrai suivi communautaire, et s'il pouvait il y aurait du personnel OVHcloud dédié à cela. J'espère que cela arrivera un jour !

## Meet & Greet

Après les nombreuses sessions, le fameux Meet & Greet a permis de continuer les discussions autour de quelques verres et buffet.

Le fait d'être à la Cité des Sciences à permis au visiteurs de profiter de quelques expositions pendant la soirée, et c'était très chouette ! (Se retrouver à chanter du Amel Bent avec la team DevOb's "pour la science" est assez improbable).

Avec pour terminer un DJ mettant l'ambiance, la soirée s'est très bien terminée, même si je suis plus team "je reste au bar pour discuter tranquille" avec du son moins fort.


## Pour finir

Première édition réussie !

J'ai beaucoup aimé pouvoir échanger avec la team OVH sans langue de bois, qui était très humble et preneur d'infos pour améliorer ses produits.

Côté "people", j'ai bien aimé discuter avec les autres speakers, certains que j'ai croisé déjà plusieurs fois, toujours présents avec le même enthousiasme. Et échanger avec ses pairs sur des sujets techs n'a pas de prix.

J'attends aussi la suite pour le côté communautaire d'OVHcloud, que cette première édition ne soit que le début d'une longue série.

Rendez-vous en 2024 j'espère !