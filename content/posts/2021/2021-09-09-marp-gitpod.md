---
title: "Créer des slides depuis n'importe où avec Marp, GitLab Pages et Gitpod"
date: 2021-09-09T12:00:00Z
thumbnail: /2021/09/2021-09-09-marp.png
categories:
  - Divers
tags:
  - GitLab
  - Markdown
description: Créer des slides depuis n'importe où avec Marp, GitLab Pages et Gitpod.
toc: true
---

Avec mon job actuel, je fais quelques présentations publiques. Comme la plupart des gens aiment travailler à distance, nous allons voir aujourd'hui comment écrire et publier des slides depuis n'importe où, pour n'importe qui !

Ci-après je vous explique comment vous pouvez écrire des slides en Markdown, puis générer un site web statique automatiquement avec les outils suivants :

* [Marp](https://marpit.marp.app/): Un écosystème pour générer des présentations en Markdown.
* [Gitpod](https://www.gitpod.io/): Un éditeur de code en ligne, de type "vscode-as-a-service", avec des workspaces automatiquement provisionnés.
* [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/): Un job de CI/CD permettant de générer un site statique avec GitLab

Pour ce faire, vous aurez besoin d'un compte GitLab et Gitpod.

## Introduction Marp

Marp est un "écosystème de présentation Markdown", qui vous permet de vous concentrer sur le fond de vos présentations en utilisant la syntaxe Markdown, en fournissant des outils pour les convertir facilement en PDF, HTML, etc. Vous pouvez utiliser des styles prédéfinis, les customiser ou bien créer vos propres styles pour la génération des documents.

Par exemple, le code suivant génèrera les slides affichées sur le côté.

![Marp Markdown avec une prévisualisation dans VS Code](/2021/09/2021-09-09-marp-01.png)

Je ne vais pas parler de comment utiliser Marp pour créer de belles présentations dans cet article, seulement comment utiliser les outils de conversion efficacement.

Vous pouvez convertir du Markdown avec les différents outils Marp : CLI (marp-cli), Extension VS Code (marp-vscode).

Pour utiliser l'extension VS Code, il suffit simplement de l'installer, CQFD :

![Marp for VS Code extension](/2021/09/2021-09-09-marp-02.png)

Maintenant, vous pouvez directement exporter les slides depuis votre éditeur favori.

![Marp export](/2021/09/2021-09-09-marp-03.png)

Vous pouvez également utiliser la CLI pour automatiser le processus de build, ce que nous allons voir juste après.

## GitLab et Gitpod pour créer les slides

OK, maintenant que vous savez comment créer des présentations depuis du Markdown, nous allons utiliser GitLab pour y stocker notre code Markdown, mais aussi utiliser Gitpod pour avoir un environnement avec tout ce qu'il faut pour générer les slides.

Concernant GitLab, créez juste un dépot, commit et poussez vos slides Markdown, et tout le reste (images, CSS, etc.).

Maintenant, nous allons créer une instance Gitpod. Grâce à son intégration dans GitLab depuis la version 13.4, vous n'avez qu'à clicliquercker sur le menu d'édition et choisir "Gitpod".

Pour avoir une intégration complète Marp, créez un fichier `.gitpod.yml` à la racine du projet avec les informations ci-après, ainsi le workspace Gitpod chargera automatiquement les extensions vscode et CLI (utilisant l'image docker officielle) :

```yaml
tasks:
  - name: Add marp-cli container
    init: docker pull marpteam/marp-cli
  - name: Configure aliases
    command: |
      alias marp='docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -e MARP_USER="$(id -u):$(id -g)" marpteam/marp-cli'
vscode:
  extensions:
   # Fix version to be compatible with current Gitpod workspace
   - marp-team.marp-vscode@1.3.0
```

![GitLab Gitpod IDE](/2021/09/2021-09-09-marp-04.png)

Cela provisionnera un workspace avec l'extension Marp, mais aussi la commande `marp` dans le terminal. Vous devrez vous logger et accepter le lien entre Gitpod et GitLab.

![Gitpod Workspace avec Marp](/2021/09/2021-09-09-marp-05.png)

Vous pouvez utiliser la commande `marp` depuis le terminal, qui est un alias vers le conteneur Docker :

![Gitpod Terminal](/2021/09/2021-09-09-marp-06.png)

*Note*: Attention avec les extensions, vous devrez utilisez un version compatible avec vs code embarqué de Gitpod pour éviter ce genre de message :

![Version incompatible](/2021/09/2021-09-09-marp-07.png)

## Publication automatique avec les GitLab Pages

A ce stade, vous pouvez lancer automatiquement une instance Gitpod depuis votre dépôt GitLab, y stocker vos présentations Markdown et les générer au format que vous souhaitez via l'extension VS Code ou la CLI... Mais il reste une tâche manuelle : la génération et la publication des slides.

Nous allons régler celà grâce à GitLab CI/CD et les GitLab Pages.

Tout d'abord, créez un pipeline qui va générer les slides en HTML :

```yaml
stages:
  - build

generate-deck:
  stage: build
  image:
    name: marpteam/marp-cli
    entrypoint: [""]
  script :
    - mkdir build/
    - export MARP_USER="$(id -u):$(id -g)"
    - /home/marp/.cli/docker-entrypoint -I . -o build/
    - /home/marp/.cli/docker-entrypoint -I . --pdf -o build/
  artifacts:
    paths:
      - "build/"
```

*Note*: vous devrez surcharger l'entrypoint de l'image Docker pour pouvoir lancer la commande depuis la CI.

Cela génèrera les fichiers HTML et PDF dan le dossier `build/`, et les rendra accessible en tant qu'artefact.

Ensuite, vous pouvez activer les GitLab Pages pour exposer ces pages HTML directement sur Internet :

```yaml
stages:
  - build
  - deploy
[...]
pages:
  stage: deploy
  script:
    # Just move the pages to the public dir
    - mv build public
  artifacts:
    paths:
      - public
  dependencies:
    - generate-deck
```

![Pipeline CI/CD](/2021/09/2021-09-09-marp-08.png)

Attendez quelques minutes... et voilà ! Vous pouvez accéder aux pages GitLab qui hébergent vos slides en HTML depuis n'importe où !

![Demo Page Web](/2021/09/2021-09-09-marp-09.png)

## Conclusion

Nous avons vu un démo rapide sur comment utiliser une suite d'outils complète pour écrire des présentations avec Marp, utiliser Gitpod pour avoir un éditeur de texte accessible facilement, et les outils GitLab CI/CD et Pages pour automatiser les process de génération et publication.

Vous pouvez accéder au code de démo [ici](https://gitlab.com/rverchere/marp-gitpod-demo). N'hésitez pas à le forker et expérimenter !


## Références

* [Set up a GitLab.com Civo Kubernetes integration with GitPod (from everywhere)](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/82)
