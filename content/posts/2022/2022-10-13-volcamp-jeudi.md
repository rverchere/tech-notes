---
title: "Notes Volcamp 2022 - Jeudi"
date: 2022-10-13T23:59:59Z
thumbnail: /2022/10/2022-10-13-volcamp.png
categories:
  - Conférence
tags:
  - Volcamp
  - IT
  - Développement
  - Kubernetes
description: Retour sur la conférence Volcamp 2022, 1er jour.
toc: true
---

![Chaine des Puys](/2022/10/2022-10-13-chainedespuys.jpg)

## Volcamp, première journée

Pendant 2 jours s'est tenue la conférence Volcamp, "Première conférence tech au coeur des volcans d'Auvergne".

Suite à une première édition en distanciel l'an dernier, c'est une deuxième édition, mais la première en présentiel.

J'ai été agréablement surpris d'être sélectionné pour y donner une présentation sur "Velero : Sauvegardez et Restaurez (proprement) vos applications kubernetes".

Les organisateurs sont tous des bénévoles du coin, et nous ont concocté un programme au poil !

Ci après un petit récap des sessions du premier jour auxquelles j'ai pu assister.

## Keynote - (et si on apprenait à) Apprendre et partager autrement

[Aurélie Vache](https://twitter.com/aurelievache) de la société OVHCloud, un peu stressée pour sa première keynote, nous explique comment, après avoir abandonné ses passions d'ados en début de carrière, a sur les reprendre pendant le COVID, en prenant du temps pour elle pour "gribouiller" pendant cette période un peu spéciale. 

Elle se confie sur son parcours pas évident d'ado bègue, et comment elle en est arriver à sortir un bouquin, faire des confs, jusqu'à cette keynote, en remerciant celles & ceux qui l'ont aider jusque là.

En deuxième partie de session elle nous donne quelques pistes pour que nous aussi puissions nous libérer du temps, et reprendre nos passions d'ados, en s'aérant l'esprit.

Tout le monde peut partager, aider, transmettre, sans pression, comme on le sent. Il ne faut pas copier les autres, ni penser qu'on n'a rien à apporter. On dira, montrera les choses toujours sous un angle différent qui pourra toucher certaines personnes.

Enfin, elle insiste sur le fait de rester nous mêmes, ne pas en faire trop, à la course aux likes sur les réseaux sociaux. Le partage doit rester un acte d'altruisme.

![Aurelie Vache](/2022/10/2022-10-13-apprendre-partager-autrement.jpg)

### Commentaires

> J'ai trouvé son témoignage émouvant, on sentait la sincérité dans son discours. Sur la suite, cela était plein de bon sens et un beau témoignage sur le partage.
>
> En "off", des collègues ne la connaissant pas ont dit avoir "pris des pieds au cul", se reconnaissant pas mal dans son parcours, qui ne touche pas que les techs.
>
> Point bonus, j'ai eu la chance d'avoir son bouquin dédicacé 😉

## Keynote - Soyons Fashion - Parlons de souveraineté technologique

[Stephane Messika](https://twitter.com/stephmessika) de la société Cleyrop propose 4 scénarios futuristes sur l'utilisation de la technologie et les problématiques d'accès aux données niveau mondial, sous couvert de souveraineté.

On parle de taxe cloud côté US, chipsest Espion côté Chine, Gestion d'une pandémie, puis survéillance de masse pour atteindre 0 criminalité.

Dans chaque cas se pose la question du calcul bénéfice/risque d'apporter de la techno contre ces menaces, tout en gardant notre indépendance.
Pour éviter ces scénarios, doit on légiférer ? Investir ? Choisr ses combats ? Coopérer entre pays ?
Pleins de questions auxquelles nous n'avons pas de réponses actuellement, mais qui amène à réfléchir...

Un digression est faite sur le HDH, avec les choix fait d'aller sur AWS à un instant T, la problématique de sécurité des données étant resolvable techniquement ainsi, versus des législations complexes inter-état qui dépasse les techs.

On parle enfin French Tech, dont un des rôles justement est d'aider à répondre à ces sujets, en essayant de rester pragmatique.

### Commentaires

> Le discours était fluide, Stéphane connait son sujet, et j'ai découvert pendant le talk qu'il faisait partie de Gaia-X, qui pour moi est un espace assez nébuleux.
> 
> Le talk était un peu trop abstrait pour moi, qui préfère les rex plus techniques.

## Comment j'ai développé le détecteur de deepfakes le plus puissant du monde pour 100€

[Mathis Hammel](https://twitter.com/MathisHammel) nous présente sur super calculateur pour détecter des deepfakes. L'enjeu est de faire tourner le tout sur une carte nvidia jetson nano, en gros un raspberry pi + gpu ;)

L'exercice proposé ici est de vérifier si une photo est réelles, ou bien générée depuis https://thispersondoesnotexist.com

Quelques concepts d'IA sont abordés comme les réseaux de neuronnes, comment entrainer l'IA, les générateurs vs discriminateurs, etc.

Il nous explique ensuite comment détecter qu'un visage est faux : position des yeux, floutage d'arrière plan, hash des images, etc.

On détaille enfin le projet avec l'utilisaton d'un scrapper d'images qui enrichi une base Elastic, l'utilisation de facenet, et comment mettre bout à bout tout cela pour reconnaitre un vrai visage d'un faux.

![Mathis Hammel](/2022/10/2022-10-13-detecteur-deep-fake.jpg)

### Commentaires

> Je voulais au départ aller voir "Service Level "Observability" - in Service Quality we Trust !", mais la salle étant pleine je me suis rabatu sur cette session.
>
> C'est toujours les sujets que je maitrise le moins qui me surprennent le plus ;)
>
> On peut voir ici que ce n'est pas super complexe pour faire de la détection, avec du bon sens et l'utilisation de bons outils on peut faire des choses sympa sans gros budget... Alors imaginez lorsque vous avez accès à des ressources d'envergure.
>
>J'ai passé un agréable moment à écouté Mathis, il rend les technos qu'il maitrise facilement compréhensibles. Bravo !

## La sécurité dès la conception du projet

[David Aparicio](https://twitter.com/dadideo) d'OVHCloud nous présente toutes les étapes de la conception de projet où la sécurité doit être implémentée, pour ne pas avoir de surprises après la mise en prod.
Et évidemment, nombreuses sont les étapes à respecter !

Après quelques exemples de non respect de la mise en oeuvre de bonnes pratiques de sécu, David nous dresse une longue liste, mais non exhaustive, des règles à respecter. Plein de cas divers traités comme les configurations par défaut, l'exposition de données personnelles, la gestion du trafic réseau entrant / sortant, etc.

Sont présentés ensuite une autre longue liste d'outil, et à quel moment du process de développement de produits on les met en place.

Avec un tour assez complet des outils & règles, il nous rappelle que la sécurité, comme la documentation, ne doit pas se faire en fin de projet. Et d'ajouter que si la doc n'est pas faite, certes ce n'est pas une bonne chose, mais pourra être plus facilement éditable qu'un logiciel "troué" qui par en prod.

![David Aparicio](/2022/10/2022-10-13-securite-projets.jpg)

### Commentaires

> J'avais loupé cette présentation à plusieurs reprises, je me suis dis que cette fois, c'était la bonne !
David fait un tour très (voir trop) complet des solutions, avec des exemples et rex à l'appui.
> 
> Bon tour du propriétaire, mais attention, on sait qu'il y a BEAUCOUP de choses à dire sur le sujet, il faut savoir faire des choix pour ne pas noyer l'auditoire.

## La grande démission - est-ce qu'on va tous changer de job en 2022 ?

Session moins tech, on échange avec [Damien Cavailles](https://twitter.com/TheDamfr) sur la démission en environnement tech.

Damien nous expose d'abord quelques faits et raisons sur les démissions actuelles, à coup de "cool / pas cool", comme le contrat psychologique, la mauvaise gestion du travail à distance avec le covid, les risques de burnout, etc.

On a de la "chance" dans nos métiers (du moins, actuellement), de pouvoir facilement démissionner pour aller voir ailleurs.

Mais faut-il encore démissionner pour les bonnes raisons ! A l'inverse des idées préconçues, il considère qu'il vaut mieux démissionner quand tout va bien, plutôt que d'attendre le point de non retour, et partir fâché. Le monde tech étant petit, ce n'est jamais bon signe.

Il propose ensuite 5 critères d'évaluations de nos entreprises, pour vérifier si l'on est bien : salaires, maturité DevOps, culture du blame & espaces safes.

![Damien Cavailles](/2022/10/2022-10-13-grande-demission.jpg)

### Commentaires

> Curieux de connaitre la personne, qui anime We Love Devs, j'ai trouvé la forme "cool", sur le fond je ne partage pas complètement son avis. Peut être que c'est à nuancer, mais je trouve le coup de démissionner quand tout va bien assez particulier comme concept, même si dans certains cas cela est justifié (proposition qu'on ne peut refuser).
>
> J'ai aussi du mal avec le concept que les démissionnaires sont de bon embassadeurs d'une boite qui va bien. Mais si cela fonctionne pour certains, tant mieux !


## Service Mesh et Kubernetes - la fausse bonne idée ?

On va parler d'un sujet qui m'intéresse, les services mesh, présenté par [Thomas Rannou](https://twitter.com/thomas_rannou), [Kevin Beaugrand](https://twitter.com/kbeaugrand) !

En prenant comme exemple une application d'e-commerce, avec Istio comme solution mise en oeuvre pour expliquer les concepts du services mesh, on fait le tour du propriétaire.

On utilisera du service mesh pour :
- De l'observabilité
- De la gestion de connexion (re-routage de flux, méchanismes de retry)
- De la sécurité (mTLS)
- Du contrôle de release (canary / blue-green)

Les exemples s'enchainent pour chacun des points, avec l'utilisation de Kiali, la présention de la gestion de retry & canary relaese avec les services spécifiques Istio.

Après le tour du propriétaire, l'équipe explique les limites de l'outil, et la surcharge que cela peut apporter au setup (composants supplémentaires, complexité, maintenace).

Selon le cas d'utilisation, on préconise de ne pas forcément passer directement sur du service mesh, mais parfois utiliser des solutions alternatives plus légères. Si toutefois le service mesh vous intéresse, on aborde l'adoption des features au fur & à mesure.

![Service Mesh](/2022/10/2022-10-13-service-mesh.jpg)

### Commentaires

> Le talk était assez simple de compréhension, très bien pour des gens qui se pose la question s'ils doivent y aller ou pas.
Juste dommage que l'application de démo ait planté ;)

## When "The little three goldies" play in the captain kube's zoo

Un conte revisité pour nous parler de chaos engineering, c'est le pari de [Jérôme Masson](https://twitter.com/sphinxgaiaone) !

J'avais eu le privilège de voir la conf de Jérôme au Camping des speakers, sans captation (et sans réseau !!)

Je vous invite alors à lire [mes précédentes notes](/posts/2022/2022-06-20-camping-des-speakers/#quand-les-trois-petits-goldies-jouent-dans-le-zoo-de-capitain-kube-%C3%A9dition-slasher).

![Jérôme Masson](/2022/10/2022-10-13-litmus.jpg)

### Commentaires

> Jérôme, la prochaine fois, arrête de faire des démos qui foirent ;)

## Apério IoT

Pour le dernier talk de la journée, la fine équipe [Thierry Chantier](https://twitter.com/titimoby), [Philippe Charrière](https://twitter.com/k33g_org) & [Louis Tournayre](https://twitter.com/_louidji) nous propose une présentation tout en détente sur quelques technos et objets IoT.

Louis étant novice, un échange question / réponse / démo se fait avec Thierry et Philippe, chacun montrant ses "jouets" connectés.
Le tout se fait autour d'un verre et charcuterie, on sent la fin de la journée !

![Apero IoT](/2022/10/2022-10-13-apero-iot.jpg)

### Commentaires

> J'ai eu quelques retours sur le côté un peu trop dilétente de la présentation, qui dénote par rapport aux précédents talks qui étaient très majoritairement bien préparés et exécutés. 
>
> Malgré cette remarque, ce fut un moment de détente agréable en fin de journée, ça ne fait pas de mal ! Surtout qu'étant au 1er rang, nous avons eu droit **aussi** à l'apéro !

## Soirée

Le déplacement à Clermont-Ferrand est aussi l'occasion de rencontrer physiquement mes collègues, qui sont venus en force pour l'événement.

C'est donc naturellement que nous avons trinqué ensemble à notre rencontre !

Cela dit, il a fallu être sage, j'avais une conf à réviser pour le lendemain...

> *Note* : comme dans toutes les régions de France, on y boit et mange bien 😅